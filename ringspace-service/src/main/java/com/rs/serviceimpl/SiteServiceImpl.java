package com.rs.serviceimpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs.bean.Site;
import com.rs.dao.SiteDao;
import com.rs.service.SiteService;

/**
 * @author HJW
 * 站点管理服务的实现类
 *
 */

@Service
public class SiteServiceImpl implements SiteService {

	@Autowired
	private SiteDao siteDao;

	@Override
	/**页面初次加载查询所有站点信息**/
	public List<Site> loadFirst() {
		List<Site> list=siteDao.loadFirst();
		return list;
	}
	
	@Override
	/**添加站点信息**/
	public void insertSite(Site Site) {
		siteDao.insertSite(Site);
	}
	
	
	@Override
	/**查询要修改的站点信息**/
	public Site getSite(int SiteId) {
		Site tc=siteDao.getSite(SiteId);
		return tc;
	}
	
	@Override
	/**修改站点信息**/
	public void updateSite(Site Site) {
		siteDao.updateSite(Site);
	}
	
	@Override
	/**删除站点信息**/
	public void deleteSite(int SiteId) {
		siteDao.deleteSite(SiteId);
	}
	
	
	/**
	 * 查询所有站点
	 */
	@Override
	public List<Site> SiteList() {
		// TODO Auto-generated method stub
		Map<String,Object> map=new HashMap<String,Object>();

		List<Site> tsite=siteDao.SiteList();
		
		/*map.put("list", tsite);
		map.put("currentPage", pageInfo.getPageNum());//当前页
		map.put("totalPage", pageInfo.getPages());//总页数
		map.put("totalPage", pageInfo.getTotal());//总记录数
		sqlSession.close();*/
		return tsite;
	}
	/**
	 * 新增站点
	 */
	@Override
	public int addsite(Site tsite) {
		// TODO Auto-generated method stub
		
		int a=siteDao.addsite(tsite);

		return a;
	}
	/**
	 * 通过id删除当前列数据
	 */        
	@Override
	public int deletesite(int siteId) {
		// TODO Auto-generated method stub
		
		int a=siteDao.deletesite(siteId);

		return a;
	}
	/**
	 * 修改站点
	 */
	@Override
	public int updatesite(Site tsite) {
		// TODO Auto-generated method stub
	
		int a=siteDao.updatesite(tsite);
	
		return a;
	}
	/**
	 * 通过id查询当前列数据
	 */
	@Override
	public Site selectsite(int siteId) {
		// TODO Auto-generated method stub
	
		Site a=siteDao.selectsite(siteId);

		return a;
	}
}
