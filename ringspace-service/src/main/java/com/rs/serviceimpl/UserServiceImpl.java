package com.rs.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs.bean.User;
import com.rs.dao.UserDao;
import com.rs.service.UserService;

/** 
* @ClassName: UserServiceImpl 
* @Description: TODO(UserService业务层的实现类) 
* @author LH
* @date 2017年10月20日 上午9:16:24 
*  
*/
@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDao userDao;

	/* (非 Javadoc) 
	* <p>Title: getUserPasswordByNickname</p> 
	* <p>Description: 根据昵称登录时查询是否有该条记录</p> 
	* @param nickname
	* @return 
	* @see com.rs.service.UserService#getUserPasswordByNickname(java.lang.String) 
	*/
	@Override
	public User getUserByNickname(String nickname){
		User result = userDao.getUserByNickname(nickname);
		return result;
	}

	
	
	/* (非 Javadoc) 
	* <p>Title: checkNickname</p> 
	* <p>Description: 验证密码唯一</p> 
	* @param nickname
	* @return 
	* @see com.rs.service.UserService#checkNickname(java.lang.String) 
	*/
	@Override
	public String checkNickname(String nickname) {
		String result=userDao.checkNickname(nickname);
		return result;
	}

	/* (非 Javadoc) 
	* <p>Title: checkPhone</p> 
	* <p>Description:注册时验证手机号唯一 </p> 
	* @param phone 手机号
	* @return 
	* @see com.rs.service.UserService#checkPhone(java.lang.String) 
	*/
	@Override
	public String checkPhone(String phone) {
		String result = userDao.checkPhone(phone);
		return result;
	}


	/* (非 Javadoc) 
	* <p>Title: saveUser</p> 
	* <p>Description: 保存用户对象</p> 
	* @param user
	* @return 插入对象的主键
	* @see com.rs.service.UserService#saveUser(com.rs.bean.User) 
	*/
	@Override
	public int saveUser(User user) {
		int num=userDao.saveUser(user);
		int result=user.getUserId();
		System.out.println(result+"主键");
		return result;
	}

}
