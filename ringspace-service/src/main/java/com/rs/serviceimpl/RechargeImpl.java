package com.rs.serviceimpl;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.rs.bean.Recharge;

import com.rs.dao.RechargeDao;
import com.rs.service.RechargeIService;

/**
 * @author Jinglaji 充值管理
 *
 */
@Service
public class RechargeImpl implements RechargeIService {

	@Autowired
	private RechargeDao rechargeDao;

	// 新增充值记录信息
	@Override
	public int addRecharge(Recharge recharge) {
		int a = rechargeDao.addRecharge(recharge);
		return a;
	}

	@Override
	public List<Recharge> selectRechargeList() {
		List<Recharge> rechargeList = rechargeDao.selectRechargeList();
		return rechargeList;
	}

}
