package com.rs.serviceimpl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.rs.bean.RolloverOrder;

import com.rs.dao.RolloverOrderDao;
import com.rs.service.RolloverOrderIService;

/**
 * @author Jinglaji 财务查询订单明细
 *
 */
@Service
public class RolloverOrderImpl implements RolloverOrderIService {

	@Autowired
	private RolloverOrderDao rolloverOrderDao;

	@Override
	public List<RolloverOrder> selectRolloverOrderList() {
		List<RolloverOrder> rolloverOrderList = rolloverOrderDao.selectRolloverOrderList();
		return rolloverOrderList;
	}

}
