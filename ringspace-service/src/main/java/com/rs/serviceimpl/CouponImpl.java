package com.rs.serviceimpl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.rs.bean.Coupon;
import com.rs.dao.CouponDao;
import com.rs.service.CouponIService;

/**
 * @author Jinglaji 卡券管理
 *
 */
@Service
public class CouponImpl implements CouponIService {

	@Autowired
	private CouponDao couponDao;

	// 新增卡券信息
	@Override
	public int addCoupon(Coupon coupon) {
		int a = couponDao.addCoupon(coupon);
		return a;
	}

	// 根据选择id查询
	@Override
	public Coupon selectCouponById(int coupon_id) {
		Coupon a = couponDao.selectCouponById(coupon_id);
		return a;
	}

	// 删除卡券信息
	@Override
	public int deleteCouponById(int coupon_id) {
		int a = couponDao.deleteCouponById(coupon_id);
		return a;
	}

	// 修改卡券信息
	@Override
	public int updateCouponById(Coupon coupon) {
		int a = couponDao.updateCouponById(coupon);
		return 0;
	}

	// 查看所有卡券信息 
	@Override
	public List<Coupon> selectCouponList() {
		List<Coupon> coupon = couponDao.selectCouponList();
		return coupon;
	}

}
