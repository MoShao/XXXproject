package com.rs.serviceimpl;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rs.service.TSiteIService;
import com.rs.bean.TSiteEmp;

import com.rs.dao.TSiteEmpDao;

@Service
public class TSiteImpl implements TSiteIService {

	/*@Autowired
	private SqlSessionFactory sqlsessionfaction;*/
	/*
	 * 上个注解是依赖注入SqlSessionFactory得到sqlsessionfaction对象，
	 * 根据对象得到sqlsession，然后通过反射得到代理对象执行方法。最新方法可以直接注入TSiteEmpDao得到代理对象tsitedao；
	 * 
	@Autowired
	private TSiteEmpDao tsitedao;*/
	

	/*@Override
	public int insert(TSiteEmp tse) {
		SqlSession sqlsession =sqlsessionfaction.openSession();
		TSiteEmpDao tsitedao=sqlsession.getMapper(TSiteEmpDao.class);
		int a =tsitedao.insert1(tse);
		
		sqlsession.close();

		return a;
		

	}*/
	@Autowired
	private TSiteEmpDao tsitedao;
	@Override
	public int insert(TSiteEmp tse) {
		//SqlSession sqlsession =sqlsessionfaction.openSession();
		//TSiteEmpDao tsitedao=sqlsession.getMapper(TSiteEmpDao.class);
		int a =tsitedao.insert1(tse);
		
		//sqlsession.close();

		return a;
		

	}
	/**
	 * 获取所有的员工列表
	 * 
	 * @return
	 */
	@Override
	public List<TSiteEmp> listEmp() {
		// TODO Auto-generated method stub
		//SqlSession sqlsession =sqlsessionfaction.openSession();
		//TSiteEmpDao tsitedao =sqlsession.getMapper(TSiteEmpDao.class);
		List<TSiteEmp> lists =tsitedao.listEmp1();		
		
		
		//sqlsession.close();
				
		return lists ;
	}
	/**
	 * 根据id删除员工
	 * 
	 * @return
	 */
	@Override
	public int delete(int siteEmpId) {
		// TODO Auto-generated method stub
		//SqlSession sqlsession =sqlsessionfaction.openSession();
		//TSiteEmpDao tsitedao =sqlsession.getMapper(TSiteEmpDao.class);
		int a =tsitedao.delete1(siteEmpId);
		
       
		
		//sqlsession.close();
		
		return a;
	}
	
	/**
	 * 修改员工信息
	 * 
	 * @return
	 */
	@Override
	public int update(TSiteEmp tsiteemp) {
		// TODO Auto-generated method stub
		//SqlSession sqlsession =sqlsessionfaction.openSession();
		//TSiteEmpDao tsitedao =sqlsession.getMapper(TSiteEmpDao.class);
		int a =tsitedao.update(tsiteemp);
        
		
		//sqlsession.close();
		
		
		return a;
	}
	/**
	 * 根据员工id显示一个员工的数据
	 * 
	 * @return
	 */
	
	@Override
	public TSiteEmp getUpdate1(int siteEmpId) {
		// TODO Auto-generated method stub
		TSiteEmp tsiteemp =tsitedao.getUpdate(siteEmpId);
		
		return tsiteemp;
	}
	/**
	 * 根据员工姓名查询员工的数据
	 * 
	 * @return
	 */
	@Override
	public TSiteEmp select(String siteEmpName) {
		// TODO Auto-generated method stub
		TSiteEmp tsiteemp=tsitedao.select(siteEmpName);
		
		
		return tsiteemp;
	}


	
}
