package com.rs.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs.bean.PhoneCode;
import com.rs.dao.CodeDao;
import com.rs.service.CodeService;

/** 
* @ClassName: CodeServiceImpl 
* @Description: TODO(codeService实现类) 
* @author LH
* @date 2017年10月21日 上午11:57:40 
*  
*/
@Service
public class CodeServiceImpl implements CodeService{
	
	/**
	 * 依赖注入codeDao
	 */
	@Autowired
	private CodeDao codeDao;
	
	
	/* (非 Javadoc) 
	* <p>Title: saveCode</p> 
	* <p>Description: </p> 
	* @param phone 手机号
	* @param code 验证码
	* @return 
	* @see com.rs.service.CodeService#saveCode(java.lang.String, java.lang.String) 
	*/
	@Override
	public int saveCode(PhoneCode phoneCode) {
		int result=codeDao.saveCode(phoneCode);
		return result;
	}


	/* (非 Javadoc) 
	* <p>Title: getCodeByPhone</p> 
	* <p>Description: 通过手机号查询验证码</p> 
	* @param phone
	* @return 
	* @see com.rs.service.CodeService#getCodeByPhone(java.lang.String) 
	*/
	@Override
	public String getCodeByPhone(String phone) {
		String result = codeDao.getCodeByPhone(phone);
		return result;
	}


	/* (非 Javadoc) 
	* <p>Title: updateCode</p> 
	* <p>Description:根据手机号更新验证码 </p> 
	* @param phoneCode
	* @return 
	* @see com.rs.service.CodeService#updateCode(com.rs.bean.PhoneCode) 
	*/
	@Override
	public int updateCode(PhoneCode phoneCode) {
		int result = codeDao.updateCode(phoneCode);
		return result;
	}


	/* (非 Javadoc) 
	* <p>Title: deleteCode</p> 
	* <p>Description:根据手机号删除一条记录 </p> 
	* @param phone
	* @return 
	* @see com.rs.service.CodeService#deleteCode(java.lang.String) 
	*/
	@Override
	public int deleteCode(String phone) {
		int result = codeDao.deleteCode(phone);
		return result;
	}

}
