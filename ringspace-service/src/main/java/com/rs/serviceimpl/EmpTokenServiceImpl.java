package com.rs.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs.bean.EmpToken;
import com.rs.bean.UserToken;
import com.rs.dao.EmpTokenDao;
import com.rs.service.EmpTokenService;



/** 
* @ClassName: EmpTokenServiceImpl 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author LH
* @date 2017年10月17日 上午10:35:01 
*  
*/
@Service
public class EmpTokenServiceImpl implements EmpTokenService{

	/**
	 * 依赖注入empTokenDao
	 */
	@Autowired
	private EmpTokenDao empTokenDao;
	
	/* (非 Javadoc) 
	* <p>Title: insertEmpToken</p> 
	* <p>Description: </p> 
	* @param empToken EmpToken实体类
	* @return 受影响的行数
	* @see com.rs.service.EmpTokenService#insertEmpToken(com.rs.bean.EmpToken) 
	*/
	@Override
	public int insertEmpToken(EmpToken empToken) {
		int result = empTokenDao.insertEmpToken(empToken);
		return result;
	}

	/* (非 Javadoc) 
	* <p>Title: getEmpToken</p> 
	* <p>Description: </p> 
	* @param empId
	* @return 
	* @see com.rs.service.EmpTokenService#getEmpToken(java.lang.String) 
	*/
	@Override
	public EmpToken getEmpToken(String empId) {
		EmpToken result = empTokenDao.getEmpToken(empId);
		return result;
	}

	
	/* (非 Javadoc) 
	* <p>Title: updateToken</p> 
	* <p>Description: </p> 
	* @param token
	* @param empId
	* @return 
	* @see com.rs.service.EmpTokenService#updateToken(java.lang.String, java.lang.String) 
	*/
	@Override
	public int updateToken(String token, String empId) {
		int result=empTokenDao.updateToken(token, empId);
		return result;
	}

	/* (非 Javadoc) 
	* <p>Title: getEmpTokenByToken</p> 
	* <p>Description: </p> 
	* @param token
	* @return 
	* @see com.rs.service.EmpTokenService#getEmpTokenByToken(java.lang.String) 
	*/
	@Override
	public EmpToken getEmpTokenByToken(String token) {
		EmpToken result=empTokenDao.getEmpTokenByToken(token);
		return result;
	}

	
}
