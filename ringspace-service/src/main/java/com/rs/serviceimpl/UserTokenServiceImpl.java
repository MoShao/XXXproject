package com.rs.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs.bean.UserToken;
import com.rs.dao.UserTokenDao;
import com.rs.service.UserTokenService;

@Service
public class UserTokenServiceImpl implements UserTokenService{

	@Autowired
	private UserTokenDao userTokenDao;
	
	@Override
	public UserToken getUserTokenByNickname(String nickname) {
		UserToken result = userTokenDao.getUserTokenByNickname(nickname);
		return result;
	}

	@Override
	public int addUserToken(UserToken userToken) {
		int result=userTokenDao.addUserToken(userToken);
		return result;
	}

	@Override
	public int updateToken(String token, String nickname) {
		int result =userTokenDao.updateToken(token, nickname);
		return result;
	}
	
	@Override
	public UserToken getUserTokenByToken(String token) {
		UserToken result=userTokenDao.getUserTokenByToken(token);
		return result;
	}

}
