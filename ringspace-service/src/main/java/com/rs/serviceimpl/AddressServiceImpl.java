package com.rs.serviceimpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs.bean.Address;
import com.rs.dao.AddressDao;
import com.rs.service.AddressService;

/**
 * @author HJW
 * 地址信息管理管理服务的实现类
 */
@Service
public class AddressServiceImpl implements AddressService{
	
	@Autowired
	private AddressDao addressDao;
	
	@Override
	/**页面初次加载查询所有地址信息**/
	public List<Address> listAddress() {
		List<Address> list=addressDao.listAddress();
		return list;
	}
	
	@Override
	/**添加地址信息**/
	public void insertAddress(Address address) {
		//获取check值，保证默认地址唯一；
		int check=address.getAddressCheck();
		if (check==1) {
			addressDao.updateCheck();
		}
		addressDao.insertAddress(address);
	}
	
	
	@Override
	/**查询要修改的地址信息**/
	public Address getAddress(String addressId) {
		Address address=addressDao.getAddress(addressId);
		return address;
	}
	
	@Override
	/**修改地址信息**/
	public void updateAddress(Address address) {
		//获取check值，保证默认地址唯一；
		int check=address.getAddressCheck();
		if (check==1) {
			addressDao.updateCheck();
		}
		addressDao.updateAddress(address);
	}
	
	@Override
	/**删除城市信息**/
	public void deleteAddress(String addressId) {
		addressDao.deleteAddress(addressId);
	}

}
