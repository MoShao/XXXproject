package com.rs.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs.service.TManagerEmpIService;
import com.rs.bean.TManagerEmp;
import com.rs.dao.TManagerEmpDao;


@Service
public class TManagerEmpImpl implements TManagerEmpIService {
	private static final String manger = null;
	@Autowired
	private TManagerEmpDao tmanagerempdao;

	@Override
	public List<TManagerEmp> managerEmpList() {
		// TODO Auto-generated method stub		
		
		List<TManagerEmp>list =tmanagerempdao.managerEmpList();
		
		return list;
	}

	@Override
	public int insert(TManagerEmp tme) {
		// TODO Auto-generated method stub
		int a =tmanagerempdao.insert(tme );
		
		return a;
	}

	@Override
	public String delete(String empId) {
		// TODO Auto-generated method stub
		int a = tmanagerempdao.delete2(empId);
		String manager="";
		if(a>0){			 
			manager ="删除成功";			
		}else{
			manager =null;
		}		
		return manager;
	}

	@Override
	public TManagerEmp select(String empName) {
		// TODO Auto-generated method stub
		TManagerEmp tme=tmanagerempdao.select(empName);
		
		return tme;
	}

}
