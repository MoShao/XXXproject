package com.rs.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs.bean.Appointment_order;
import com.rs.bean.Cargo_type;
import com.rs.dao.AppointmentDao;
import com.rs.service.AppointmentService;
@Service
public class AppointmentImpl implements AppointmentService {
	@Autowired
	 private AppointmentDao appointmentDao;


	@Override
	public int insertAppointment(Appointment_order appointment) {
		// TODO Auto-generated method stub
		return appointmentDao.insertAppointment(appointment);
		
	}


	@Override
	public List<Cargo_type> selectCargo_type() {
		// TODO Auto-generated method stub
		
		return appointmentDao.selectCargo_type();
	}


	@Override
	public List<Appointment_order> selectAppointment_order() {
		// TODO Auto-generated method stub
		return appointmentDao.selectAppointment_order();
	}
	
	
	
}
