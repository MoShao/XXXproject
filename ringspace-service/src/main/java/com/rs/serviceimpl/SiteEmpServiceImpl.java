package com.rs.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs.bean.SiteEmp;
import com.rs.dao.SiteEmpDao;
import com.rs.service.SiteEmpService;

/**
 * @author hexin
 * 实现接口，重写方法
 *
 */
@Service
public class SiteEmpServiceImpl implements SiteEmpService {

	@Autowired
	private SiteEmpDao siteEmpDao;
	
	/**
	 * 查询所有站点员工
	 */
	@Override
	public List<SiteEmp> siteEmpList() {
		// TODO Auto-generated method stub
		//Map<String,Object> map=new HashMap<String,Object>();
		
		List<SiteEmp> siteEmp=siteEmpDao.siteEmpList();
		
		return siteEmp;
	}
	/**
	 * 新增站点员工
	 */
	@Override
	public int addSiteEmp(SiteEmp siteEmp) {
		// TODO Auto-generated method stub
		int a=siteEmpDao.addSiteEmp(siteEmp);
			
		return a;
	}
	/**
	 * 通过id删除当前列数据
	 */
	@Override
	public int deleteSiteEmp(int siteEmpId) {
		// TODO Auto-generated method stub
		int a=siteEmpDao.deleteSiteEmp(siteEmpId);
		
		return a;
	}
	/**
	 * 修改站点员工
	 */
	@Override
	public int updateSiteEmp(SiteEmp siteEmp) {
		// TODO Auto-generated method stub
		int a=siteEmpDao.updateSiteEmp(siteEmp);
		return a;
	}
	/**
	 * 通过id查询当前列数据
	 */
	@Override
	public SiteEmp selectSiteEmp(int siteEmpId) {
		// TODO Auto-generated method stub
		
		SiteEmp a=siteEmpDao.selectSiteEmp(siteEmpId);
		return a;
	}

}
