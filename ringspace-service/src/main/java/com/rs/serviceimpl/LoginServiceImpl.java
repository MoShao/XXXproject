package com.rs.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs.bean.ManagerEmp;
import com.rs.dao.LoginDao;
import com.rs.service.LoginService;

/**
 * @author LH
 * 管理层员工业务层实现类
 *
 */
@Service
public class LoginServiceImpl implements LoginService{
	
	/**
	 * 依赖管理层员工dao层
	 */
	@Autowired
	private LoginDao ManagerEmpDao;
	
	/* (non-Javadoc)
	 * @see com.rs.service.ManagerEmpService#login(java.lang.String)
	 */
	@Override
	public ManagerEmp login(String empId) {
		
		ManagerEmp result = ManagerEmpDao.login(empId);
		return result;
	}
	
	

}
