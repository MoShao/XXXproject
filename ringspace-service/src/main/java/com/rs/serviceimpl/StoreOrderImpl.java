package com.rs.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



import com.rs.bean.StoreOrder;

import com.rs.dao.StoreOrderDao;
import com.rs.service.StoreOrderIService;

/**
 * @author Jinglaji 财务查询订单明细
 *
 */
@Service
public class StoreOrderImpl implements StoreOrderIService {

	@Autowired
	private StoreOrderDao storeOrderDao;

	@Override
	public List<StoreOrder> selectStoreOrderList() {
		List<StoreOrder> storeOrder = storeOrderDao.selectStoreOrderList();
		return storeOrder;
	}

}
