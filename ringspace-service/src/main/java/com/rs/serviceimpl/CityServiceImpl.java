package com.rs.serviceimpl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs.bean.City;
import com.rs.dao.CityDao;
import com.rs.service.CityService;

/**
 * @author HJW
 * 鍩庡競绠＄悊鏈嶅姟鐨勫疄鐜扮被
 */
@Service
public class CityServiceImpl implements CityService{
	
	@Autowired
	private CityDao cityDao;
	
	@Override
	/**椤甸潰鍒濇鍔犺浇鏌ヨ鎵�鏈夊煄甯備俊鎭�**/
	public List<City> listCity() {    
		List<City> list=cityDao.listCity();
		return list;
	}
	
	@Override
	/**娣诲姞鍩庡競淇℃伅**/
	public void insertCity(City city) {
		cityDao.insertCity(city);
	}
	
	
	@Override
	/**鏌ヨ瑕佷慨鏀圭殑鍩庡競淇℃伅**/
	public City getCity(String cityId) {
		City tc=cityDao.getCity(cityId);
		return tc;
	}
	
	@Override
	/**淇敼鍩庡競淇℃伅**/
	public void updateCity(City city) {
		cityDao.updateCity(city);
	}
	
	@Override
	/**鍒犻櫎鍩庡競淇℃伅**/
	public void deleteCity(String cityId) {
		cityDao.deleteCity(cityId);
	}

	@Override
	public String getCityByName(String city, String province) {

		String result = cityDao.getCityByName(city, province);
		return result;
	}


}
