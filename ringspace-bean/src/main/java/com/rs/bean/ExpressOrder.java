package com.rs.bean;

import java.util.Date;

/**
 * @author Jinglaji 快递订单表
 *
 */
public class ExpressOrder {
	private String express_id; //快递订单编号
	private String site_emp_id; //员工编号
	private String user_id; //用户编号
	private String cargo_id; //货物编号
	private String express_sendname; //寄件人姓名
	private String express_sendphone; //寄件人联系方式
	private String express_receivename; //收件人姓名
	private String express_receiveaddress; //收货地址
	private String express_receivephone; //收件人联系方式
	private double express_price; //订单价格
	private String express_paytype; //订单支付方式
	private Date express_date; //下单时间
	private String express_strdate;

	public String getExpress_id() {
		return express_id;
	}

	public void setExpress_id(String express_id) {
		this.express_id = express_id;
	}

	public String getSite_emp_id() {
		return site_emp_id;
	}

	public void setSite_emp_id(String site_emp_id) {
		this.site_emp_id = site_emp_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getCargo_id() {
		return cargo_id;
	}

	public void setCargo_id(String cargo_id) {
		this.cargo_id = cargo_id;
	}

	public String getExpress_sendname() {
		return express_sendname;
	}

	public void setExpress_sendname(String express_sendname) {
		this.express_sendname = express_sendname;
	}

	public String getExpress_sendphone() {
		return express_sendphone;
	}

	public void setExpress_sendphone(String express_sendphone) {
		this.express_sendphone = express_sendphone;
	}

	public String getExpress_receivename() {
		return express_receivename;
	}

	public void setExpress_receivename(String express_receivename) {
		this.express_receivename = express_receivename;
	}

	public String getExpress_receiveaddress() {
		return express_receiveaddress;
	}

	public void setExpress_receiveaddress(String express_receiveaddress) {
		this.express_receiveaddress = express_receiveaddress;
	}

	public String getExpress_receivephone() {
		return express_receivephone;
	}

	public void setExpress_receivephone(String express_receivephone) {
		this.express_receivephone = express_receivephone;
	}

	public double getExpress_price() {
		return express_price;
	}

	public void setExpress_price(double express_price) {
		this.express_price = express_price;
	}

	public String getExpress_paytype() {
		return express_paytype;
	}

	public void setExpress_paytype(String express_paytype) {
		this.express_paytype = express_paytype;
	}

	public Date getExpress_date() {
		return express_date;
	}

	public void setExpress_date(Date express_date) {
		this.express_date = express_date;
	}

	public String getExpress_strdate() {
		return express_strdate;
	}

	public void setExpress_strdate(String express_strdate) {
		this.express_strdate = express_strdate;
	}

}
