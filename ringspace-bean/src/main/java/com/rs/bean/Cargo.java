package com.rs.bean;

import java.math.BigDecimal;
import java.util.Date;

public class Cargo {
	
	private String cargoId;
	
	private String userId;
	
	private int cargoType;
	
	private String cargoName;
	
	private BigDecimal cargoWeught;
	
	private BigDecimal CargoValue;
	
	private Date creatrTime;

	public String getCargoId() {
		return cargoId;
	}

	public void setCargoId(String cargoId) {
		this.cargoId = cargoId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getCargoType() {
		return cargoType;
	}

	public void setCargoType(int cargoType) {
		this.cargoType = cargoType;
	}

	public String getCargoName() {
		return cargoName;
	}

	public void setCargoName(String cargoName) {
		this.cargoName = cargoName;
	}

	public BigDecimal getCargoWeught() {
		return cargoWeught;
	}

	public void setCargoWeught(BigDecimal cargoWeught) {
		this.cargoWeught = cargoWeught;
	}

	public BigDecimal getCargoValue() {
		return CargoValue;
	}

	public void setCargoValue(BigDecimal cargoValue) {
		CargoValue = cargoValue;
	}

	public Date getCreatrTime() {
		return creatrTime;
	}

	public void setCreatrTime(Date creatrTime) {
		this.creatrTime = creatrTime;
	}

}
