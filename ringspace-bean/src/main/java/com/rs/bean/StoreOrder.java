package com.rs.bean;

import java.util.Date;

/**
 * @author Jinglaji 存储订单表
 *
 */
public class StoreOrder {
	private String store_id; // 1.存取订单编号
	private String site_emp_id; // 2.业务员编号
	private String user_id; // 3.用户编号
	private String user_name; // 4.交易人姓名
	private String cargo_id; // 5.货物编号
	private String payid; // 6.订单交易号
	private String contacts_name; // 7.联系人姓名
	private String contacts_phone; // 8.联系方式
	private String contacts_addr; // 9.联系地址
	private double amount; // 10.订单金额
	private String paytype; // 11.支付方式
	private Date store_time; // 12.下单时间
	private String store_timestr; // 12.下单时间

	public String getStore_id() {
		return store_id;
	}

	public void setStore_id(String store_id) {
		this.store_id = store_id;
	}

	public String getSite_emp_id() {
		return site_emp_id;
	}

	public void setSite_emp_id(String site_emp_id) {
		this.site_emp_id = site_emp_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getCargo_id() {
		return cargo_id;
	}

	public void setCargo_id(String cargo_id) {
		this.cargo_id = cargo_id;
	}

	public String getPayid() {
		return payid;
	}

	public void setPayid(String payid) {
		this.payid = payid;
	}

	public String getContacts_name() {
		return contacts_name;
	}

	public void setContacts_name(String contacts_name) {
		this.contacts_name = contacts_name;
	}

	public String getContacts_phone() {
		return contacts_phone;
	}

	public void setContacts_phone(String contacts_phone) {
		this.contacts_phone = contacts_phone;
	}

	public String getContacts_addr() {
		return contacts_addr;
	}

	public void setContacts_addr(String contacts_addr) {
		this.contacts_addr = contacts_addr;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getPaytype() {
		return paytype;
	}

	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}

	public Date getStore_time() {
		return store_time;
	}

	public void setStore_time(Date store_time) {
		this.store_time = store_time;
	}

	public String getStore_timestr() {
		return store_timestr;
	}

	public void setStore_timestr(String store_timestr) {
		this.store_timestr = store_timestr;
	}

}
