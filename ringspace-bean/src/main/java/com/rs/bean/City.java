package com.rs.bean;

public class City {
   
    private String cityId;

    private String cityName;

    private String cityPerson;

    private String cityNumber;

    private String cityProvince;

   
    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName == null ? null : cityName.trim();
    }

    public String getCityPerson() {
        return cityPerson;
    }

 
    public void setCityPerson(String cityPerson) {
        this.cityPerson = cityPerson == null ? null : cityPerson.trim();
    }

 
    public String getCityNumber() {
        return cityNumber;
    }

    public void setCityNumber(String cityNumber) {
        this.cityNumber = cityNumber == null ? null : cityNumber.trim();
    }

    public String getCityProvince() {
        return cityProvince;
    }

    public void setCityProvince(String cityProvince) {
        this.cityProvince = cityProvince == null ? null : cityProvince.trim();
    }
}