package com.rs.bean;

import java.util.Date;

/**
 * @author Jinglaji 卡券实体类
 *
 */
public class Coupon {

	private int coupon_id;
	private String coupon_type;
	private int coupon_number;
	private Date coupon_starttime;
	private int coupon_useing;

	public int getCoupon_id() {
		return coupon_id;
	}

	public void setCoupon_id(int coupon_id) {
		this.coupon_id = coupon_id;
	}

	public String getCoupon_type() {
		return coupon_type;
	}

	public void setCoupon_type(String coupon_type) {
		this.coupon_type = coupon_type;
	}

	public int getCoupon_number() {
		return coupon_number;
	}

	public void setCoupon_number(int coupon_number) {
		this.coupon_number = coupon_number;
	}

	public Date getCoupon_starttime() {
		return coupon_starttime;
	}

	public void setCoupon_starttime(Date coupon_starttime) {
		this.coupon_starttime = coupon_starttime;
	}

	public int getCoupon_useing() {
		return coupon_useing;
	}

	public void setCoupon_useing(int coupon_useing) {
		this.coupon_useing = coupon_useing;
	}

}