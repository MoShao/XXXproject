package com.rs.bean;

public class Appointment_order {

	private String appointment_id;
	private String cargo_type_id;
	private String user_id;
	private String appointment_name;
	private String appointment_tel;
	private String appointment_address;
	private String appointment_province;
	private String appointment_city;
	private String appointment_area;
	private String remark;
	private String status="待预约";
	private String appointment_pare_time;

	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getAppointment_id() {
		return appointment_id;
	}
	public void setAppointment_id(String appointment_id) {
		this.appointment_id = appointment_id;
	}
	public String getAppointment_pare_time() {
		return appointment_pare_time;
	}
	public void setAppointment_pare_time(String appointment_pare_time) {
		this.appointment_pare_time = appointment_pare_time;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAppointment_name() {
		return appointment_name;
	}
	public void setAppointment_name(String appointment_name) {
		this.appointment_name = appointment_name;
	}
	public String getCargo_type_id() {
		return cargo_type_id;
	}
	public void setCargo_type_id(String cargo_type_id) {
		this.cargo_type_id = cargo_type_id;
	}
	public String getAppointment_tel() {
		return appointment_tel;
	}
	public void setAppointment_tel(String appointment_tel) {
		this.appointment_tel = appointment_tel;
	}
	public String getAppointment_address() {
		return appointment_address;
	}
	public void setAppointment_address(String appointment_address) {
		this.appointment_address = appointment_address;
	}
	public String getAppointment_province() {
		return appointment_province;
	}
	public void setAppointment_province(String appointment_province) {
		this.appointment_province = appointment_province;
	}
	public String getAppointment_city() {
		return appointment_city;
	}
	public void setAppointment_city(String appointment_city) {
		this.appointment_city = appointment_city;
	}
	public String getAppointment_area() {
		return appointment_area;
	}
	public void setAppointment_area(String appointment_area) {
		this.appointment_area = appointment_area;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
		return "Appointment_order [cargo_type_id=" + cargo_type_id + ", appointment_name=" + appointment_name
				+ ", appointment_tel=" + appointment_tel + ", appointment_address=" + appointment_address
				+ ", appointment_province=" + appointment_province + ", appointment_city=" + appointment_city
				+ ", appointment_area=" + appointment_area + ", remark=" + remark + ", status=" + status
				+ ", appointment_pare_time=" + appointment_pare_time + "]";
	}
	
	
}
