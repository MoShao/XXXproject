package com.rs.bean;

import java.math.BigDecimal;

public class ManagerEmp {
	
	private int id;//序号
	private String empId;//工号
	private String password;//密码
	private String deptId;//部门编号
	private String empName;//员工姓名
	private String empPhone;//员工手机
	private String empIdcard;//省份证号
	private String empAddress;//员工地址
	private String empSex;//性别
	private String empjob;//职位
	private BigDecimal empSalary;//薪资
	private String emp_remark;//备注
	private String photo; 
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpPhone() {
		return empPhone;
	}
	public void setEmpPhone(String empPhone) {
		this.empPhone = empPhone;
	}
	public String getEmpIdcard() {
		return empIdcard;
	}
	public void setEmpIdcard(String empIdcard) {
		this.empIdcard = empIdcard;
	}
	public String getEmpAddress() {
		return empAddress;
	}
	public void setEmpAddress(String empAddress) {
		this.empAddress = empAddress;
	}
	public String getEmpSex() {
		return empSex;
	}
	public void setEmpSex(String empSex) {
		this.empSex = empSex;
	}
	public String getEmpjob() {
		return empjob;
	}
	public void setEmpjob(String empjob) {
		this.empjob = empjob;
	}
	public BigDecimal getEmpSalary() {
		return empSalary;
	}
	public void setEmpSalary(BigDecimal empSalary) {
		this.empSalary = empSalary;
	}
	public String getEmp_remark() {
		return emp_remark;
	}
	public void setEmp_remark(String emp_remark) {
		this.emp_remark = emp_remark;
	}

}
