package com.rs.bean;

import java.util.Date;

public class Address {
    
    private String addressId;
    private String userId;
    private String addressProvince;
    private String addressCity;
    private String addressArea;
    private String addressDetaile;
    private String addressPerson;
    private String addressNumber;
    private int addressCheck;
    private Date mgtCreate;
    private Date mgtModified;
    
    public String getAddressId() {
        return addressId;
    }
    
    public void setAddressId(String addressId) {
        this.addressId = addressId == null ? null : addressId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getAddressProvince() {
        return addressProvince;
    }

    public void setAddressProvince(String addressProvince) {
        this.addressProvince = addressProvince == null ? null : addressProvince.trim();
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity == null ? null : addressCity.trim();
    }

    public String getAddressArea() {
        return addressArea;
    }

    public void setAddressArea(String addressArea) {
        this.addressArea = addressArea == null ? null : addressArea.trim();
    }

    public String getAddressDetaile() {
        return addressDetaile;
    }

    public void setAddressDetaile(String addressDetaile) {
        this.addressDetaile = addressDetaile == null ? null : addressDetaile.trim();
    }

    public String getAddressPerson() {
        return addressPerson;
    }

    public void setAddressPerson(String addressPerson) {
        this.addressPerson = addressPerson == null ? null : addressPerson.trim();
    }

    public String getAddressNumber() {
        return addressNumber;
    }

    public void setAddressNumber(String addressNumber) {
        this.addressNumber = addressNumber == null ? null : addressNumber.trim();
    }

    public int getAddressCheck() {
		return addressCheck;
	}

	public void setAddressCheck(int addressCheck) {
		this.addressCheck = addressCheck;
	}

	public Date getMgtCreate() {
        return mgtCreate;
    }

    public void setMgtCreate(Date mgtCreate) {
        this.mgtCreate = mgtCreate;
    }

    public Date getMgtModified() {
        return mgtModified;
    }

    public void setMgtModified(Date mgtModified) {
        this.mgtModified = mgtModified;
    }
}