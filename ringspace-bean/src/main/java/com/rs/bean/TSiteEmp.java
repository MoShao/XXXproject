package com.rs.bean;

import java.math.BigDecimal;
import java.sql.Date;


public class TSiteEmp {
    private Integer siteEmpId;

    private Integer siteId;

    private String siteEmpName;

    private String siteEmpPhone;

    private String siteEmpIdcard;

    private String siteEmpAddress;

    private String siteEmpSex;

   

	private BigDecimal siteEmpSalary;

    private String siteEmpRemark;

    private String siteEmpJob;
    

    public TSiteEmp() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TSiteEmp(Integer siteEmpId, Integer siteId, String siteEmpName, String siteEmpPhone, String siteEmpIdcard,
			String siteEmpAddress, String siteEmpSex, BigDecimal siteEmpSalary,
			String siteEmpRemark, String siteEmpJob) {
		super();
		this.siteEmpId = siteEmpId;
		this.siteId = siteId;
		this.siteEmpName = siteEmpName;
		this.siteEmpPhone = siteEmpPhone;
		this.siteEmpIdcard = siteEmpIdcard;
		this.siteEmpAddress = siteEmpAddress;
		this.siteEmpSex = siteEmpSex;
		
		this.siteEmpSalary = siteEmpSalary;
		this.siteEmpRemark = siteEmpRemark;
		this.siteEmpJob = siteEmpJob;
	}

	public Integer getSiteEmpId() {
        return siteEmpId;
    }

    public void setSiteEmpId(Integer siteEmpId) {
        this.siteEmpId = siteEmpId;
    }

    public Integer getSiteId() {
        return siteId;
    }

    public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }

    public String getSiteEmpName() {
        return siteEmpName;
    }

    public void setSiteEmpName(String siteEmpName) {
        this.siteEmpName = siteEmpName == null ? null : siteEmpName.trim();
    }

    public String getSiteEmpPhone() {
        return siteEmpPhone;
    }

    public void setSiteEmpPhone(String siteEmpPhone) {
        this.siteEmpPhone = siteEmpPhone == null ? null : siteEmpPhone.trim();
    }

    public String getSiteEmpIdcard() {
        return siteEmpIdcard;
    }

    public void setSiteEmpIdcard(String siteEmpIdcard) {
        this.siteEmpIdcard = siteEmpIdcard == null ? null : siteEmpIdcard.trim();
    }

    public String getSiteEmpAddress() {
        return siteEmpAddress;
    }

    public void setSiteEmpAddress(String siteEmpAddress) {
        this.siteEmpAddress = siteEmpAddress == null ? null : siteEmpAddress.trim();
    }

    public String getSiteEmpSex() {
        return siteEmpSex;
    }

    public void setSiteEmpSex(String siteEmpSex) {
        this.siteEmpSex = siteEmpSex == null ? null : siteEmpSex.trim();
    }

   

    public BigDecimal getSiteEmpSalary() {
        return siteEmpSalary;
    }

    public void setSiteEmpSalary(BigDecimal siteEmpSalary) {
        this.siteEmpSalary = siteEmpSalary;
    }

    public String getSiteEmpRemark() {
        return siteEmpRemark;
    }

    public void setSiteEmpRemark(String siteEmpRemark) {
        this.siteEmpRemark = siteEmpRemark == null ? null : siteEmpRemark.trim();
    }

    public String getSiteEmpJob() {
        return siteEmpJob;
    }

    public void setSiteEmpJob(String siteEmpJob) {
        this.siteEmpJob = siteEmpJob == null ? null : siteEmpJob.trim();
    }
}