package com.rs.bean;

import java.util.Date;

/**
 * @author Jinglaji 充值实体类
 *
 */
public class Recharge {
	private String recharge_id;
	private String user_id;
	private double recharge_money;
	private String recharge_paytype;
	private Date recharge_date;

	public String getRecharge_id() {
		return recharge_id;
	}

	public void setRecharge_id(String recharge_id) {
		this.recharge_id = recharge_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public double getRecharge_money() {
		return recharge_money;
	}

	public void setRecharge_money(double recharge_money) {
		this.recharge_money = recharge_money;
	}

	public String getRecharge_paytype() {
		return recharge_paytype;
	}

	public void setRecharge_paytype(String recharge_paytype) {
		this.recharge_paytype = recharge_paytype;
	}

	public Date getRecharge_date() {
		return recharge_date;
	}

}