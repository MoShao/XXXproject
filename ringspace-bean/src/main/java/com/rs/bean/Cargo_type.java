package com.rs.bean;

public class Cargo_type {
	private int cargo_type_id;
	private String cargo_type_name;
	public int getCargo_type_id() {
		return cargo_type_id;
	}
	public void setCargo_type_id(int cargo_type_id) {
		this.cargo_type_id = cargo_type_id;
	}
	public String getCargo_type_name() {
		return cargo_type_name;
	}
	public void setCargo_type_name(String cargo_type_name) {
		this.cargo_type_name = cargo_type_name;
	}
	
	
	
}
