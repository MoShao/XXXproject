package com.rs.bean;

public class Site {
    private Integer siteId;

    private Integer deptId;

    private String siteName;

    private String siteProniace;

    private String siteCity;

    private String siteLinkman;
    
    private String sitePhone;

	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getSiteProniace() {
		return siteProniace;
	}

	public void setSiteProniace(String siteProniace) {
		this.siteProniace = siteProniace;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteLinkman() {
		return siteLinkman;
	}

	public void setSiteLinkman(String siteLinkman) {
		this.siteLinkman = siteLinkman;
	}

	public String getSitePhone() {
		return sitePhone;
	}

	public void setSitePhone(String sitePhone) {
		this.sitePhone = sitePhone;
	}
    
    

}