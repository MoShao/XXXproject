package com.rs.bean;

import java.util.Date;

/**
 * @author Jinglaji 转仓订单表
 *
 */
public class RolloverOrder {
	private String rollover_id; // 转仓订单编号
	private String cargo_id; // 货物编号
	private String paytype; // 支付方式
	private String user_id; // 用户编号
	private String rollover_dest_id; // 目标站点编号
	private String rollover_start_id; // 发出站点编号
	private double rollover_price; // 订单金额
	private Date rollover_time; // 下单时间
	private String rollover_timestr; // 下单时间

	public String getRollover_id() {
		return rollover_id;
	}

	public void setRollover_id(String rollover_id) {
		this.rollover_id = rollover_id;
	}

	public String getCargo_id() {
		return cargo_id;
	}

	public void setCargo_id(String cargo_id) {
		this.cargo_id = cargo_id;
	}

	public String getPaytype() {
		return paytype;
	}

	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getRollover_dest_id() {
		return rollover_dest_id;
	}

	public void setRollover_dest_id(String rollover_dest_id) {
		this.rollover_dest_id = rollover_dest_id;
	}

	public String getRollover_start_id() {
		return rollover_start_id;
	}

	public void setRollover_start_id(String rollover_start_id) {
		this.rollover_start_id = rollover_start_id;
	}

	public double getRollover_price() {
		return rollover_price;
	}

	public void setRollover_price(double rollover_price) {
		this.rollover_price = rollover_price;
	}

	public Date getRollover_time() {
		return rollover_time;
	}

	public void setRollover_time(Date rollover_time) {
		this.rollover_time = rollover_time;
	}

	public String getRollover_timestr() {
		return rollover_timestr;
	}

	public void setRollover_timestr(String rollover_timestr) {
		this.rollover_timestr = rollover_timestr;
	}

	
}
