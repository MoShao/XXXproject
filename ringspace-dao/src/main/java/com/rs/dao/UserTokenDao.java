package com.rs.dao;

import org.apache.ibatis.annotations.Param;

import com.rs.bean.UserToken;

/** 
* @ClassName: UserTokenDao 
* @Description: TODO(用户tokendao层) 
* @author LH
* @date 2017年10月20日 下午2:04:25 
*  
*/
public interface UserTokenDao {
	
	/** 
	* @Title: getUserTokenByNickname 
	* @Description: TODO(根据用户昵称查找UserToken) 
	* @param nickname 昵称
	* @return UserToken 用户yoken 
	* @throws 
	*/
	UserToken getUserTokenByNickname(String nickname);
	
	
	/** 
	* @Title: addUserToken 
	* @Description: TODO(添加一条用户的token信息) 
	* @param userToken 
	* @return int    受影响的行数
	* @throws 
	*/
	int addUserToken(UserToken userToken);
	
	/** 
	* @Title: updateToken 
	* @Description: TODO(根据昵称修改口令) 
	* @param token 口令
	* @param nickname 昵称
	* @return int    返回类型  受影响的行数
	* @throws 
	*/
	int updateToken(@Param("token")String token,@Param("nickname")String nickname); 
	
	
	/** 
	* @Title: getUserTokenByToken 
	* @Description: TODO(根据口令获取信息) 
	* @param token 口令
	* @return UserToken    返回类型 
	* @throws 
	*/
	UserToken getUserTokenByToken(String token);

}
