package com.rs.dao;

import org.apache.ibatis.annotations.Param;

import com.rs.bean.EmpToken;

/** 
* @ClassName: EmpTokenDao 
* @Description: TODO(EmpToken Dao层) 
* @author LH
* @date 2017年10月17日 上午10:37:29 
*  
*/
public interface EmpTokenDao {
	
	/** 
	* @Title: insertEmpToken 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param empToken EmpToken 实体类
	* @return int 受影响的行数   返回类型 
	* @throws 
	*/
	int insertEmpToken(EmpToken empToken);
	
	/** 
	* @Title: getEmpToken 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param empId 工号
	* @return EmpToken    对象
	* @throws 
	*/
	EmpToken getEmpToken(String empId);
	
	/** 
	* @Title: updateToken 
	* @Description: TODO(根据工号修改token值) 
	* @param token 口令
	* @param empId 工号
	* @return int    返回类型 
	* @throws 
	*/
	int updateToken(@Param("token") String token,@Param("empId") String empId); 
	
	/** 
	* @Title: getEmpTokenByToken 
	* @Description: TODO(根据口令获取所有信息) 
	* @param token
	* @return    参数
	* @return EmpToken    返回类型 
	* @throws 
	*/
	EmpToken getEmpTokenByToken(String token);
	
}
