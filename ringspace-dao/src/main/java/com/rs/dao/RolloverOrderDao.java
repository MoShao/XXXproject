package com.rs.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.rs.bean.RolloverOrder;

/**
 * @author Jinglaji 转仓订单查询
 *
 */
public interface RolloverOrderDao {

	// 财务入口查询快递订单
	/*List<RolloverOrder> selectRolloverOrderList( @Param("express_id") String express_id,@Param("express_paytype")String express_paytype);*/
	List<RolloverOrder> selectRolloverOrderList();
}
