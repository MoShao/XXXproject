package com.rs.dao;

import com.rs.bean.User;

public interface UserDao {
	
	/** 
	* @Title: getUserPasswordByNickname 
	* @Description: TODO(通过昵称获取密码) 
	* @param userNickname 昵称
	* @return String    返回类型  密码
	* @throws 
	*/
	User getUserByNickname(String nickname);
	
	
	
	/** 
	* @Title: checkNickname 
	* @Description: TODO(注册时验证昵称唯一) 
	* @param userNickname  昵称
	* @return int    是否存在
	* @throws 
	*/
	String checkNickname(String nickname);
	
	/** 
	* @Title: checkPhone 
	* @Description: TODO(注册时验证手机号唯一) 
	* @param phone
	* @return    参数
	* @return String    返回类型 
	* @throws 
	*/
	String checkPhone(String phone);
	
	/** 
	* @Title: saveUser 
	* @Description: TODO(注册是保存一条记录) 
	* @param user 要添加的对象
	* @return int  插入的条数
	* @throws 
	*/
	int saveUser(User user);

}
