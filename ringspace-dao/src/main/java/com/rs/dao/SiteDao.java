package com.rs.dao;

import java.util.List;
import com.rs.bean.Site;

/**
 * @author HJW 分站管理具体业务接口
 */
public interface SiteDao {

	/**
	 * 页面初次加载查询所有站点信息
	 * 
	 * @return 站点信息list
	 */
	List<Site> loadFirst();

	/**
	 * 添加站点信息
	 * 
	 * @param site
	 *            单条站点信息
	 */
	void insertSite(Site site);

	/**
	 * 查询要修改的站点信息
	 * 
	 * @param siteId
	 *            站点ID
	 * @return 单条站点信息
	 */
	Site getSite(int siteId);

	/**
	 * 修改站点信息
	 * 
	 * @param site
	 *            单条站点信息
	 */
	void updateSite(Site site);

	/**
	 * 删除站点信息
	 * 
	 * @param siteId
	 *            站点ID
	 */
	void deleteSite(int siteId);

	// 查询所有站点
	List<Site> SiteList();

	// 新增站点
	int addsite(Site tsite);

	// 删除站点
	int deletesite(int siteId);

	// 修改站点
	int updatesite(Site tsite);

	// 根据id查询当前数据
	Site selectsite(int siteId);

}
