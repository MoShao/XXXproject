package com.rs.dao;

import java.util.List;
import com.rs.bean.Recharge;

/**
 * @author Jinglaji 充值管理
 *
 */
public interface RechargeDao {
	// 定义接口充值记录新增方法
	int addRecharge(Recharge recharge);

	List<Recharge> selectRechargeList();
}
