package com.rs.dao;

import java.util.List;

import com.rs.bean.Address;

/**
 * @author HJW
 *	地址信息管理业务接口
 */
public interface AddressDao {
	
	
	/**
	 * 页面初次加载查询所有地址信息
	 * @return 地址信息list
	 */
	List<Address> listAddress();
	
	
	/**
	 * 设置默认地址唯一
	 */
	void updateCheck();
	
	/**
	 * 添加地址信息
	 * @param address 单条地址信息
	 */
	void insertAddress(Address address);
	
	/**
	 * 查询要修改的地址信息
	 * @param addressId 地址ID
	 * @return 单条地址信息
	 */
	Address getAddress(String addressId);
	
	/**
	 * 修改地址信息
	 * @param address 单条地址信息
	 */
	void updateAddress(Address address);
	
	/**
	 * 删除地址信息
	 * @param addressId 地址ID
	 */
	void deleteAddress(String addressId);
	
	

}
