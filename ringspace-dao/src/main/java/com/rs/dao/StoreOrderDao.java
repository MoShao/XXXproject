package com.rs.dao;

import java.util.List;

import com.rs.bean.StoreOrder;


public interface StoreOrderDao {
	List<StoreOrder> selectStoreOrderList();
}
