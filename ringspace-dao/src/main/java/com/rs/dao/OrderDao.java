package com.rs.dao;

import java.util.List;
import java.util.Map;

import com.rs.bean.ExpressOrder;

/**
 * @author Jinglaji 订单查询
 *
 */
public interface OrderDao {


    List<ExpressOrder> selectOrderList(Map<String,String> param);
}
