package com.rs.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.rs.bean.City;

/**
 * @author HJW
 *	城市管理具体业务接口
 */
public interface CityDao {
	
	
	/**
	 * 页面初次加载查询所有城市信息
	 * @return 城市信息list
	 */
	List<City> listCity();
	
	/**
	 * 添加城市信息
	 * @param city 单条城市信息
	 */
	void insertCity(City city);
	
	/**
	 * 查询要修改的城市信息
	 * @param cityId 城市ID
	 * @return 单条城市信息
	 */
	City getCity(String cityId);
	
	/**
	 * 修改城市信息
	 * @param city 单条城市信息
	 */
	void updateCity(City city);
	
	/**
	 * 删除城市信息
	 * @param cityId 城市ID
	 */
	void deleteCity(String cityId);
	
	/** 
	* @Title: selectCityByName 
	* @Description: TODO(根据省份和城市名查找该城市负责人的empid) 
	* @param city 城市名
	* @param province 省份名
	* @return   负责人工号
	* @return String    返回类型 
	* @throws 
	*/
	String getCityByName(@Param("city")String city,@Param("province")String province);

}
