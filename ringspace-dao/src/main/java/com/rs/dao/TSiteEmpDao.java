package com.rs.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.rs.bean.TSiteEmp;

/**
 * DAO接口
 * @author wuzc
 *
 */
public interface TSiteEmpDao {
	
	
	//添加用户信息
		 int insert1(TSiteEmp tse);
	//分部员工列表查询
		 List<TSiteEmp> listEmp1();
	//根据员工id删除
		 int delete1(int siteEmpId);
	//根据员工id修改员工的信息
		 
		 int update( TSiteEmp tsiteemp);
	//修改的时候显示一列数据的dao层方法
		 TSiteEmp getUpdate(int siteEmpId);
    //输入用户名查询员工信息
		 TSiteEmp select(String siteEmpName);
}
