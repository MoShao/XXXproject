package com.rs.dao;

import com.rs.bean.ManagerEmp;

/**
 * @author LH
 * 管理员工登录
 *
 */
public interface LoginDao {
	
	
	/**
	 * @param empId 员工工号
	 * @return 员工部分信息的实体类
	 */
	ManagerEmp login(String empId);

}
