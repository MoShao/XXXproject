package com.rs.dao;

import java.util.List;

import com.rs.bean.SiteEmp;

/**
 * @author he 对分站员工的增删改查
 *
 */
public interface SiteEmpDao {
	// 查询所有员工
	List<SiteEmp> siteEmpList();

	// 新增员工
	int addSiteEmp(SiteEmp siteEmp);

	// 删除员工
	int deleteSiteEmp(int siteEmpId);

	// 修改员工信息
	int updateSiteEmp(SiteEmp siteEmp);

	// 根据id查询当前数据
	SiteEmp selectSiteEmp(int siteEmpId);

}
