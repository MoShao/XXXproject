package com.rs.dao;

import java.util.List;

import com.rs.bean.TManagerEmp;

public  interface TManagerEmpDao {
	//显示总部员工的列表	
	List<TManagerEmp> managerEmpList();
	//添加总部员工的信息
	int insert(TManagerEmp tme);
	//删除员工根据id执行
	int delete2(String empId);
	//根据员工姓名查询员工的信息
	TManagerEmp select(String empName);

}
