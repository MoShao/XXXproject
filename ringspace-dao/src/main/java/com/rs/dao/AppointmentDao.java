package com.rs.dao;

import java.util.List;

import com.rs.bean.Appointment_order;
import com.rs.bean.Cargo_type;

public interface AppointmentDao {
	
	/**
	 * @param appointment 需要存储的对象
	 * @return 影响行数
	 */
	int insertAppointment(Appointment_order appointment);
	
	/**
	 * @return 物品分配的列表
	 */
	List<Cargo_type> selectCargo_type();
	
	
	/**
	 * @return 预约表信息
	 */
	List<Appointment_order> selectAppointment_order();
}
