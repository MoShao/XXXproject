package com.rs.util;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;

/**
 * @ProjectName ringspace-parent
 * @ClassName Pager
 * @Description TODO 分页基础类
 * @Author jing_pin
 * @Date 2018/8/1 下午6:36
 * @Version 1.0
 **/
public class Pager implements Serializable {
    private void writeObject(java.io.ObjectOutputStream out) throws IOException {

    }

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {

    }

    private static int START_ROWS = 1;
    public static final int ONE_PAGE_SIZE = 1;
    public static final int TEN = 10;
    public static final int FIFTEEN = 15;
    public static final int TWENTY = 20;

    //存储结果
    private Collection<?> result;

    //总记录数
    private long totalCount;
    //当前页数
    private int page = 1;

    //总页数
    private int pageCount = 1;

    //每页显示记录数
    private int pageSize = TEN;
    //每次取的条数 等同于每页显示记录数
    private int limit = TEN;
    //开始行
    private int start;

    public Pager(long totalCount,Collection<?> result){
        this.totalCount=totalCount;
        this.result=result;
    }

    public Collection<?> getResult() {
        return result;
    }

    public void setResult(Collection<?> result) {
        this.result = result;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getStart() {
        return start;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setStart(int start) {
        this.start = start;
    }


}
