package com.rs.util;

import com.github.pagehelper.Page;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @ProjectName ringspace-parent
 * @ClassName Result
 * @Description TODO
 * @Author jing_pin
 * @Date 2018/8/1 下午3:58
 * @Version 1.0
 **/
public class Result<T> implements UserResult, Serializable {

    public static final String OK = "0";
    public static final Result<Object> SUCCESS = new Result("0", "");

    public static class Bean implements Serializable {
        int total = 0;

        public Bean(int total) {
            this.total = total;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int totalCount) {
            this.total = total;
        }
    }

    private Collection<T> beans;
    private Bean bean;
    private Object object = new Object();
    private int totalCount = 0;
    private String returnMessage = "";
    private String returnCode = "";

    public Result() {
        this(null);
    }

    /*
     *@Author jing_pin
     *Description //TODO 错误消息对象构造
     *Date  2018/8/1
     *Param
     *@return
     **/

    public Result(String returnMessage, String returnCode) {
        this(null);
        this.returnMessage = returnMessage;
        this.returnCode = returnCode;
    }

    public Result(Collection<T> dataList) {
        this(dataList, dataList != null ? dataList.size() : 0);
    }

    public Result(int totalCount) {
        this((Collection<T>) null, totalCount);
    }

    /*
     *@Author jing_pin
     *Description //TODO 分页结果构造
     *Date  2018/8/1
     *Param [dataList, totalCount]
     *@return
     **/
    public Result(Collection<T> dataList, int totalCount) {
        this.totalCount = totalCount;
        this.returnCode = Result.OK;
        this.returnMessage = "";
        this.beans = dataList;
        this.getBean().setTotal(totalCount);
    }


    public Result(Page<T> pageResult) {
        this((Collection) (pageResult != null ? pageResult.getResult() : null));
        this.setTotalCount(pageResult != null ? (int) pageResult.getTotal() : 0);
    }

    public Collection<T> getBeans() {
        if (this.beans == null) this.beans = new ArrayList<>(0);
        return beans;
    }

    public void setBeans(Collection<T> beans) {
        this.beans = beans;
    }

    public Bean getBean() {
        if (this.bean == null) this.bean = new Bean(0);
        return bean;
    }

    public void setBeans(Result.Bean bean) {
        this.bean = bean;
    }

    public int getTotalCount() {
        return this.totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }


    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }


}
