package com.rs.service;

import com.rs.bean.EmpToken;
import com.rs.bean.UserToken;


/** 
* @ClassName: EmpTokenService 
* @Description: TODO(EmpToken业务层接口) 
* @author LH
* @date 2017年10月17日 上午10:24:56 
*  
*/
public interface EmpTokenService {
	
	/** 
	* @Title: insertEmpToken 
	* @Description: TODO(向emp_token表插入一条记录) 
	* @param @param empToken
	* @param @return    参数
	* @return int    返回类型 
	* @throws 
	*/
	int insertEmpToken(EmpToken empToken);
	
	/** 
	* @Title: getEmpToken 
	* @Description: TODO(根据工号查询一条记录) 
	* @param empId
	* @return    参数
	* @return EmpToken    返回类型 
	* @throws 
	*/
	EmpToken getEmpToken(String empId);
	
	/** 
	* @Title: updateToken 
	* @Description: TODO(根据工号修改token值) 
	* @param token 口令
	* @param empId 工号
	* @return int    返回类型 
	* @throws 
	*/
	int updateToken(String token,String empId); 
	
	/** 
	* @Title: getEmpTokenByToken 
	* @Description: TODO(根据口令获取所有信息) 
	* @param token
	* @return    参数
	* @return EmpToken    返回类型 
	* @throws 
	*/
	EmpToken getEmpTokenByToken(String token);
	

}
