package com.rs.service;

import com.rs.bean.PhoneCode;

/** 
* @ClassName: CodeService 
* @Description: TODO(验证码业务层) 
* @author LH
* @date 2017年10月21日 上午11:51:10 
*  
*/
public interface CodeService {
	
	/** 
	* @Title: saveCode 
	* @Description: TODO(保存一条验证码信息) 
	* @param phoneCode 对象
	* @return int    返回类型 
	* @throws 
	*/
	int saveCode(PhoneCode phoneCode);
	
	/** 
	* @Title: getCodeByPhone 
	* @Description: TODO(根据手机号查询验证码) 
	* @param phone 手机号
	* @return Integer 验证码
	* @throws 
	*/
	String getCodeByPhone(String phone);
	
	
	/** 
	* @Title: updateCode 
	* @Description: TODO(更新验证码) 
	* @param phoneCode 实体对象
	* @return int    更新的行数
	* @throws 
	*/
	int updateCode(PhoneCode phoneCode);
	
	/** 
	* @Title: deleteCode 
	* @Description: TODO(验证完成后删除记录) 
	* @param phone 手机号
	* @return int  删除的行数
	* @throws 
	*/
	int deleteCode(String phone);

}
