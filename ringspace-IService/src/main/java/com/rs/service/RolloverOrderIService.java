package com.rs.service;

import java.util.List;

import com.rs.bean.RolloverOrder;

/**
 * @author Jinglaji 财务查询快递订单
 *
 */

public interface RolloverOrderIService {
	// 财务管理界面查询订单明细
	/*List<ExpressOrder> selectOrderList(String express_id,String express_paytype);*/
	List<RolloverOrder> selectRolloverOrderList();
}
