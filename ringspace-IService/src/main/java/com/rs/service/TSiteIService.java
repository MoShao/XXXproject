package com.rs.service;


import java.util.List;

import com.rs.bean.TSiteEmp;



public interface TSiteIService {
	

	
	//添加员工信息业务
	public int insert(TSiteEmp tse);
	//分部员工列表查询业务
	public List<TSiteEmp> listEmp();
	//根据员工id删除
	public int delete(int siteEmpId);
	//根据员工id修改，此方法是修改员工
	public int update(TSiteEmp tsiteemp);
	//根据员工id显示一个员工的数据
	TSiteEmp getUpdate1(int siteEmpId);
	//输入用户名查询员工信息
	TSiteEmp select(String siteEmpName);
}