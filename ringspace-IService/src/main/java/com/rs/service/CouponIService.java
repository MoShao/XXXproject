package com.rs.service;

import java.util.List;

import com.rs.bean.Coupon;

/**
 * @author Jinglaji 卡券管理
 *
 */
public interface CouponIService {

	int addCoupon(Coupon coupon);

	Coupon selectCouponById(int coupon_id);

	int deleteCouponById(int coupon_id);

	int updateCouponById(Coupon coupon);

	List<Coupon> selectCouponList();
}