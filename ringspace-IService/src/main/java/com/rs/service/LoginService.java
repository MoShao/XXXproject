package com.rs.service;

import com.rs.bean.ManagerEmp;

/**
 * @author LH
 * 管理层员工业务层接口
 *
 */
public interface LoginService {
	
	/**
	 * 根据员工工号查询出员工的密码，部门编号，photo，名字
	 * @param empId 员工编号
	 * @return ManagerEmp对象
	 */
	ManagerEmp login(String empId);

}
