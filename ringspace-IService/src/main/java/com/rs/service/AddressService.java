package com.rs.service;

import java.util.List;

import com.rs.bean.Address;


/**
 * @author HJW
 * 地址信息管理服务类
 *
 */
public interface AddressService {
	
	/**
	 * 页面初次加载查询所有地址信息
	 * @return
	 */
	List<Address> listAddress();
	
	/**
	 * 添加地址信息
	 * @param address 单条地址信息
	 */
	void insertAddress(Address address);
	
	/**
	 * 查询要修改的地址信息
	 * @param addressId 地址ID
	 * @return 单条地址信息
	 */
	Address getAddress(String addressId);
	
	/**
	 * 修改地址信息
	 * @param address 单条地址信息
	 */
	void updateAddress(Address address);
	
	/**
	 * 删除地址信息
	 * @param addressId 地址ID
	 */
	void deleteAddress(String addressId);

	

	

	
	
	

}

