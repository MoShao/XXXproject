package com.rs.service;

import java.util.List;

import com.rs.bean.Recharge;

/**
 * @author Jinglaji 充值管理
 *
 */
public interface RechargeIService {

	int addRecharge(Recharge recharge);

	List<Recharge> selectRechargeList();

}