package com.rs.service;

import java.util.List;

import com.rs.bean.Appointment_order;
import com.rs.bean.Cargo_type;

public interface AppointmentService {
	
	/**
	 * @param appointment 预约表单的对象
	 * @return
	 */
	int insertAppointment(Appointment_order appointment);
	
	List<Cargo_type> selectCargo_type();
	
	/**
	 * @return 预约表信息
	 */
	List<Appointment_order> selectAppointment_order();
}
