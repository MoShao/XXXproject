package com.rs.service;

import java.util.List;
import java.util.Map;
import com.rs.util.Pager;
import com.rs.bean.ExpressOrder;

/**
 * @author Jinglaji 财务查询快递订单
 *
 */

public interface OrderIService {

    com.rs.util.Pager selectOrderList(int currpage, int pageSize, Map<String,String> param);
}
