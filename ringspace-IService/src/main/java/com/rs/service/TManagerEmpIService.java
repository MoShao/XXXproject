package com.rs.service;

import java.util.List;

import com.rs.bean.TManagerEmp;

public interface TManagerEmpIService {
	//显示总部员工的列表	
	List<TManagerEmp> managerEmpList();
	//添加总部员工的信息
	int insert(TManagerEmp tme);
	//删除员工根据id执行
	String delete(String empId);
	//根据员工姓名查询员工的信息
	TManagerEmp select(String empName);

}
