package com.rs.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rs.bean.City;
import com.rs.service.CityService;

/**
 * @author HJW
 *	城市信息管理控制器
 */
@Controller
@RequestMapping("/city")
public class CityController {
	
	@Autowired
	private CityService cityService;
	
	
	/**
	 * 初次加载获取所有城市信息
	 * @param model 模型
	 * @param page 当前页
	 * @param limit 显示行数
	 * @return map
	 */
	@RequestMapping("/loadFirst")
	@ResponseBody
	public Map listCity(Model model, int page, int limit){
		Page<PageInfo> pageInfo = (Page) PageHelper.startPage(page, limit);
		List<City> list=cityService.listCity();
		Map<String,Object> map = new HashMap();
		map.put("data", list);
		map.put("code", 0);
		map.put("msg", "");
		map.put("count", pageInfo.getTotal());
		return map;
	}
	
	/**
	 * 添加城市信息
	 * @param city 单条城市信息
	 * @return 添加成功视图
	 */
	@RequestMapping("/insertCity")
	public String insertCity(City city){
		try {
			cityService.insertCity(city);
		} catch (Exception e) {
			e.printStackTrace();
			return "manager/operation/city/error";
		}
		return "manager/operation/city/addSuccess";
	}
	
	/**
	 * 查询要修改的城市信息
	 * @param cityId 城市ID
	 * @param model 模型
	 * @return 城市信息修改视图
	 */
	@RequestMapping("/getCity")
	public String getCity(String cityId,Model model){
		City city=cityService.getCity(cityId);
		model.addAttribute("city", city);
		return "manager/operation/city/city-edit";
	}
	
	/**
	 * 修改城市信息
	 * @param city 单条城市信息
	 * @return 修改成功视图
	 */
	@RequestMapping("/updateCity")
	public String updateCity(City city){
		try {
			cityService.updateCity(city);
		} catch (Exception e) {
			e.printStackTrace();
			return "manager/operation/city/error";
		}
		return "manager/operation/city/updateSuccess";
	}
	
	/**
	 * 删除城市信息
	 * @param cityId 城市ID
	 * @return 删除成功视图
	 */
	@RequestMapping("/deleteCity")
	public String deleteCity(String cityId){
		try {
			cityService.deleteCity(cityId);
		} catch (Exception e) {
			e.printStackTrace();
			return "manager/operation/city/error";
		}
		return "manager/operation/city/deleteSuccess";
	}
	
	
}
