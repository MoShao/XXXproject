package com.rs.controller;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rs.bean.Coupon;
import com.rs.service.CouponIService;

/**
 * @author Jinglaji 卡券管理
 *
 */
@Controller
@RequestMapping("/coupon")
public class CouponController {

	@Autowired
	private CouponIService couponIService;

	// 新增卡券信息
	@RequestMapping("/addCoupon")
	public void addCoupon(Coupon coupon,HttpServletResponse resp) {
		int a = couponIService.addCoupon(coupon);
		if (a > 0) {
			try {
				resp.sendRedirect("../manager/html/finance/success.html");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			try {
				resp.sendRedirect("../manager/html/finance/success.html");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


	}
	// 查询卡券信息byid

	@RequestMapping("/selectCouponById")
	public String selectCoupon(int coupon_id, Model model) {
		System.out.println(coupon_id);
		Coupon coupon = couponIService.selectCouponById(coupon_id);
		System.out.println(coupon.getCoupon_id());
		model.addAttribute("coupon", coupon);
		return "coupon-edit";

	}

	// 卡券建议不删除以后查询使用
	@RequestMapping("/deleteCouponById")
	public String deletecoupon(String coupon_id) {
		// TODO 可能要删除
		System.out.println("测试");
		System.out.println(coupon_id);
		int b = Integer.parseInt(coupon_id);
		int a = couponIService.deleteCouponById(b);
		if (a > 0) {
			return "success";

		}
		return "defeat";

	}

	// 修改卡券信息
	@RequestMapping("/updateCouponById")
	public String updateCoupon(Coupon coupon) {
		int a = couponIService.updateCouponById(coupon);
		if (a > 0) {
			return "success";
		}
		return "defeat";

	}

	// 查询所有卡券信息，添加分页信息
	/*
	 * @RequestMapping("/selectCouponList") public String selectCoupon(Model model)
	 * { List<Coupon> couponList = couponIService.selectCouponList();
	 * model.addAttribute("couponList", couponList); return "coupon-list";
	 * 
	 * }
	 */

	@RequestMapping("/selectCouponList")
	@ResponseBody
	public Map selectCoupon(Model model, int page, int limit) {
		Page<PageInfo> pageInfo = (Page) PageHelper.startPage(page, limit);
		List<Coupon> couponList = couponIService.selectCouponList();
		System.out.println(couponList.toString());
		Map<String, Object> map = new HashMap();
		map.put("data", couponList);
		map.put("code", 0);
		map.put("msg", "");
		map.put("count", pageInfo.getTotal());
		return map;

	}

	public void setCouponIService(CouponIService couponIService) {
		this.couponIService = couponIService;
	}

}
