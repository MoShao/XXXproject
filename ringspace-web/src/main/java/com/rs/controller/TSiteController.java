package com.rs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rs.service.TSiteIService;
import com.rs.bean.TSiteEmp;

/**
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/uc")
public class TSiteController {
	
	@Autowired
	private TSiteIService tsiteIService;
	
    /**  
     * 添加用户信息  
     */  
    @RequestMapping("/empadd")//为方法设置访问路径  
    public String insert(TSiteEmp tse,String siteEmpHiredate){
		int a =tsiteIService.insert(tse);
		
    	if (a>0){
    		System.out.println(a);
    		//成功界面
    		return  "manager/personnel/insertsuccess";	
    	}else{
    		System.out.println(a);
    		return "manager/personnel/error";
    	}
    	 //失败界面
    }
    /**  
     * 查看员工的列表  
     */ 
    
    @RequestMapping("/emplist")//查询员工列表的路径
    public String listEmp(Model model){
    	//调用service接口中的方法    	
    	List <TSiteEmp> empList = tsiteIService.listEmp();
    	//把值存在 model 作用域
    	model.addAttribute("empList", empList);
    	//跳转列表页面
		
		if (empList!=null){
    		//成功界面
    		return "manager/personnel/adminClient-list2";
    	}else{
    		return "manager/personnel/error";
    	}
    	
    }
	/*public void setTsiteIService(TSiteIService tsiteIService) {
		this.tsiteIService = tsiteIService;
	}*/
    /**  
     * 删除员工
     */
    @RequestMapping("/delete")//删除员工的路径
    public String delete(int siteEmpId){
    	int a =tsiteIService.delete(siteEmpId);
    	
    	if (a>0){
    		//成功界面
    		return  "manager/personnel/deletesuccess";	
    	}else{
    		return "manager/personnel/error";
    	}
		
    }
    /**  
     * 编辑员工信息的页面后，点击确认修改员工后执行的方法
     */
    @RequestMapping("/update")//修改员工的路径
    public String update(TSiteEmp tsiteemp){
    	int b =tsiteIService.update(tsiteemp);
    	//把值存在 model作用域
    	//medel.addAttribute("tsiteemp", tsiteemp);
    	
    	System.out.println(b);
		if (b>0){
    		//成功界面
			//System.out.println(b);
    		return "manager/personnel/updatesuccess";	
    	}else{
    		return "manager/personnel/error";
    	}
    	}
    
    /**  
     * 点击编辑按钮时，跳出的修改员工信息的页面
     */
    @RequestMapping("/getupdate")//根据员工id显示一个员工的数据
	public String getUpdate(int siteEmpId,Model model){
    	TSiteEmp tsiteemp1 =tsiteIService.getUpdate1(siteEmpId);
    	model.addAttribute("tsiteemp1", tsiteemp1);
		return "manager/personnel/emp-update";
    	
    }
    /**  
     * 点击查询按钮时，跳出的员工信息的页面
     */
    @RequestMapping("/selectemp")//根据员工的姓名查询员工的数据信息
    public String getSelect(String siteEmpName,Model model){
    	TSiteEmp tsiteemp = tsiteIService.select(siteEmpName);
    	model.addAttribute("tsiteemp", tsiteemp);
		return "manager/personnel/emp";
    	
    }
    
    
    
    	
}
    

    

