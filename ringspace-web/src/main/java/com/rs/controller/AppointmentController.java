package com.rs.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rs.service.AppointmentService;
import com.rs.bean.Appointment_order;
import com.rs.bean.Cargo_type;

@Controller
@RequestMapping("/appointment")
public class AppointmentController {
	
	@Autowired
	private AppointmentService appointmentService;
	
	@RequestMapping("/insertAppointment")
	public String insertAppointment(Appointment_order appointment) throws ParseException {
		appointment.setAppointment_id(System.currentTimeMillis()+"");
		appointment.setUser_id((System.currentTimeMillis()+"").substring(8));
		int result = appointmentService.insertAppointment(appointment);
		if(result>=1){
			return "user/papare/insertSuccess";
		}else{
			return "user/papare/insertError";
		}
	}
	@RequestMapping("/selectAppointment_order")
	public String selectAppointment_order(Model model) throws ParseException {
		
		List<Appointment_order> lists = appointmentService.selectAppointment_order();
		model.addAttribute("lists",lists);
		model.addAttribute("count",lists.size());
		
		return "user/papare/papareOrder-list";
	}
	/**
	 * @return 查询到的物品类别
	 */
	@RequestMapping("/selectCargo_type")
	@ResponseBody
	public List<Cargo_type> selectCargo_type(){
		System.out.println("执行了");
		List<Cargo_type> lists= appointmentService.selectCargo_type();
		return lists;
	}

}
