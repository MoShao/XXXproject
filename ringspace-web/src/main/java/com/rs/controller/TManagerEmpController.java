package com.rs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rs.service.TManagerEmpIService;
import com.rs.bean.TManagerEmp;

/**
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/manager")
public class TManagerEmpController {
	
	@Autowired
	private TManagerEmpIService tmanagerempiservice;
	/*展现员工
	的列表*/
	@RequestMapping("/emplist")//查询员工的列表信息
	public String getList(Model model){
		List<TManagerEmp> emplist=tmanagerempiservice.managerEmpList();	
		model.addAttribute("emplist", emplist);
		return "manager/personnel/adminClient-list1";
	}
	
	
	
	
	
	/**
     * 添加员工的信息
     */
	@RequestMapping("/addemp")
	public String getInsert(TManagerEmp tme){
		int a =tmanagerempiservice.insert(tme);
		System.out.println(a);
		if(a!=0){
			return "manager/personnel/insertsuccess1";
		}else{
		
		return "manager/personnel/error";
		
		}
	}
	/**
     * 根据员工的id删除员工信息
     */
	@RequestMapping("/delete")
	public String getDelete(String empId){
		String manager=tmanagerempiservice.delete(empId);
		
		if(manager!=null){
			System.out.println(manager);
			return "manager/personnel/deletesuccess1";
		}else{	
			return "manager/personnel/error";
		}		
	}
	
	@RequestMapping("/selectname")
	public String getSelect(String empName,Model model){
		TManagerEmp tme=tmanagerempiservice.select(empName);
		model.addAttribute("tme", tme);
		return "manager/personnel/manageremp";
	}
    	
}
    

    

