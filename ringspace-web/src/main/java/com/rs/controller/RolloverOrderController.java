package com.rs.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;


import com.rs.bean.RolloverOrder;
import com.rs.service.RolloverOrderIService;

/**
 * @author Jinglaji 财务入口查询
 *
 */
@Controller
@RequestMapping("/order")
public class RolloverOrderController {

	@Autowired
	private RolloverOrderIService rolloverOrderIService;

	// 查询快递订单记录
	@RequestMapping("/selectRolloverOrderList")
	@ResponseBody
	public Map selectOrder(Model model,int page,int limit) {

		Page<PageInfo> pageInfo = (Page) PageHelper.startPage(page, limit);
		List<RolloverOrder> orderList = rolloverOrderIService.selectRolloverOrderList();
		for(RolloverOrder exp:orderList) {
			Date date=exp.getRollover_time();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String date2 = format.format(date);
			exp.setRollover_timestr(date2);
		}
		Map<String, Object> map = new HashMap();
		map.put("data", orderList);
		map.put("code", 0);
		map.put("msg", "");
		map.put("count", pageInfo.getTotal());
		return map;

	}

	public void setRolloverOrderIService(RolloverOrderIService rolloverOrderIService) {
		this.rolloverOrderIService = rolloverOrderIService;
	}

}
