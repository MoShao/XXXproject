package com.rs.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rs.bean.Recharge;
import com.rs.service.RechargeIService;

@Controller
@RequestMapping("/recharge")
public class RechargeController {

	/**
	 * @author Jinglaji 充值管理
	 *
	 */

	@Autowired
	private RechargeIService rechargeIService;

	// 新增充值记录
	@RequestMapping("/addRecharge")
	public String addRecharge(Recharge recharge) {
		int a = rechargeIService.addRecharge(recharge);
		if (a > 0) {
			return "success";
		}

		return "defeat";

	}

	// 查询充值记录
	@RequestMapping("/selectRechargeList")
	@ResponseBody
	public Map selectRechargeList(Model model,int page,int limit) {
		Page<PageInfo> pageInfo=(Page)PageHelper.startPage(page,limit);
		List<Recharge> rechargeList = rechargeIService.selectRechargeList();
		Map<String,Object> map=new HashMap();
		map.put("data", rechargeList);
		map.put("code", 0);
		map.put("msg", "");
		map.put("count", pageInfo.getTotal());
		return map;

	}

	public void setRechargeIService(RechargeIService rechargeIService) {
		this.rechargeIService = rechargeIService;
	}

}
