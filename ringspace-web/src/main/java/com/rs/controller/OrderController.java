package com.rs.controller;

import com.rs.service.OrderIService;
import com.rs.util.Pager;
import com.rs.util.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Jinglaji 财务入口查询
 */
@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderIService orderIService;

    //获取页面参数
    private Map<String, String> getPageParams(HttpServletRequest request) {
        String expressId = request.getParameter("expressId");
        String expressPayType = request.getParameter("expressPayType");

        Map<String, String> map = new HashMap<>();
        if (StringUtils.isNotEmpty(expressId)) {
            map.put("expressId", expressId);
        }
        if (StringUtils.isNotEmpty(expressPayType)) {
            map.put("expressPayType", expressPayType);
        }

        return map;
    }

    //分页显示订单列表
    @RequestMapping("/getOrderList")
    @ResponseBody
    public Object getOrderList(HttpServletRequest request) {
        int start = Integer.parseInt(request.getParameter("start"));//查询开始条数
        int pageSize = Integer.parseInt(request.getParameter("limit"));//每页显示条数
        int pageIndex = start / pageSize;
        int currpage = pageIndex + 1;//当前页码
        //获取页面参数的map
        Map<String, String> param = getPageParams(request);
        Result result = new Result();
        Pager page = orderIService.selectOrderList(currpage, pageSize, param);
        result = new Result(page.getResult(), new Long(page.getPageCount()).intValue());
        return result;
    }

    public void setOrderIService(OrderIService orderIService) {
        this.orderIService = orderIService;
    }

}
