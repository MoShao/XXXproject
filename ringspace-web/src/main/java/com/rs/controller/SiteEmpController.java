package com.rs.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rs.bean.SiteEmp;
import com.rs.service.SiteEmpService;

/**
 * @author hexin
 * 分站员工管理控制层
 *
 */

@Controller
@RequestMapping("/siteEmp")
public class SiteEmpController {
	
	@Autowired
	private SiteEmpService ses;
	/**
	 * 
	 * @param model
	 * @param page
	 * @param limit
	 * @return
	 * 查询的当前员工列表和分页
	 */
	@RequestMapping("/selectSiteEmp")
	@ResponseBody
	public Map selectSiteEmp(Model model, int page, int limit) {
		Page<PageInfo> pageInfo = (Page) PageHelper.startPage(page, limit);
		List<SiteEmp> se=ses.siteEmpList();
		Map<String,Object> map = new HashMap();
		map.put("data", se);
		map.put("code", 0);
		map.put("msg", "");
		map.put("count", pageInfo.getTotal());
		return map;
		
	}
	/**
	 * 
	 * @param siteEmp
	 * @return
	 * 添加员工
	 */
	@RequestMapping("/addSiteEmp")
	public String addSiteEmp(SiteEmp siteEmp) {
		int a=ses.addSiteEmp(siteEmp);
		if (a > 0) {
			// 成功界面
			return "success";
		}
		// 失败界面
		return "defeat";
		
		
	}
	/**
	 * 
	 * @param siteEmpId
	 * @param model
	 * @return
	 * 通过id查询当前员工数据
	 */
	@RequestMapping("/selectSiteEmpById")
	public String selectSiteEmp(int siteEmpId, Model model) {
		SiteEmp se=ses.selectSiteEmp(siteEmpId);
		model.addAttribute("se",se);
		return "update2";
		
	}
	/**
	 * 
	 * @param se
	 * @return
	 * 修改员工数据
	 */
	@RequestMapping("/updateSiteEmp")
	public String updateSiteEmp(SiteEmp se) {
		
		int a=ses.updateSiteEmp(se);
		if (a > 0) {
			// 成功界面
			return "success";
		}
		// 失败界面
		return "defeat";
		
	}
	/**
	 * 
	 * @param siteEmpId
	 * @return
	 * 根据员工id删除员工数据
	 */
	@RequestMapping("/deleteSiteEmp")
	public String deleteSiteEmp(int siteEmpId) {
		
		int a=ses.deleteSiteEmp(siteEmpId);
		if (a > 0) { 
			// 成功界面
			return "success";
		}
		// 失败界面
		return "defeat";
		
	}
	

}
