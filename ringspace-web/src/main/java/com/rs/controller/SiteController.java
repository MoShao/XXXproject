package com.rs.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rs.bean.Site;
import com.rs.service.SiteService;


/**
 * 分站管理控制层
 * @author hexin
 *
 */
@Controller
@RequestMapping("/tsite")
public class SiteController {


	@Autowired
	private SiteService tsiteIService;
/**
 * 查询所有站点
 * @param model 模型
 * @param page 当前页
 * @param limit 显示行数
 * @return
 */
	
	@RequestMapping("/selectTSite")
	@ResponseBody
	public Map selectTSite(Model model, int page, int limit) {
		Page<PageInfo> pageInfo = (Page) PageHelper.startPage(page, limit);
		List<Site> tSites = tsiteIService.SiteList();
		Map<String,Object> map = new HashMap();
		map.put("data", tSites);
		map.put("code", 0);
		map.put("msg", "");
		map.put("count", pageInfo.getTotal());
		return map;
	}

	/**
	 * 查询要修改的城市信息
	 * @param siteId
	 * @param model
	 * @return
	 */
	@RequestMapping("/selectsite")
	public String selectsite(int siteId, Model model) {
		Site tsite = tsiteIService.selectsite(siteId);
		System.out.println(tsite.getSiteId());
		model.addAttribute("tsite", tsite);
		return "manager/operation/site/update1";

	}

	/**
	 * 新增站点
	 * @param tsite
	 * @return
	 */
	@RequestMapping("/addsite")
	public String addsite(Site tsite) {

		int a = tsiteIService.addsite(tsite);
		if (a > 0) {
			// 成功界面
			return "manager/operation/site/one1";
		}
		// 失败界面
		return "manager/operation/site/one2";

	}

	/**
	 *  删除站点
	 * @param siteId
	 * @return
	 */
	@RequestMapping("/deletesite")
	public String deletesite(String siteId) {
		System.out.println("dfgfdshsd");
		System.out.println(siteId);
		int b=Integer.parseInt(siteId);
		int a = tsiteIService.deletesite(b);
		if (a > 0) {
			// 成功界面
			return "manager/operation/site/one1";
		}
		// 失败界面
		return "manager/operation/site/one2";

	}

	/**
	 * 修改站点
	 * @param tsite
	 * @return
	 */
	@RequestMapping("/updatesite")
	public String updatesite(Site tsite) {
		System.out.println(tsite.getSiteId());
		System.out.println(tsite.getSiteName() + tsite.getSiteProniace() + tsite.getSiteCity() + tsite.getSiteLinkman()
				+ tsite.getSitePhone());
		int a = tsiteIService.updatesite(tsite);
		System.out.println(a);

		if (a > 0) {
			// 成功界面
			return "manager/operation/site/one1";
		}
		// 失败界面
		return "manager/operation/site/one2";

	}

}
