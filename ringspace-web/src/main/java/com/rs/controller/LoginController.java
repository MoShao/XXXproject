package com.rs.controller;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rs.bean.EmpToken;
import com.rs.bean.ManagerEmp;
import com.rs.service.CityService;
import com.rs.service.EmpTokenService;
import com.rs.service.LoginService;
import com.rs.util.CookieUtils;

/**
 * @author LH 后台管理登录
 *
 */

@Controller
@RequestMapping("/login")
public class LoginController {

	/**
	 * 依赖注入ManagerEmpService业务层
	 */
	@Autowired
	private LoginService loginService;

	/**
	 * 依赖注入EmpTokenService业务层
	 */
	@Autowired
	private EmpTokenService empTokenService;

	@Autowired
	private CityService cityService;

	/**
	 * @param empId
	 *            工号
	 * @param password
	 *            密码
	 * @param deptId
	 *            部门编号
	 * @return 返回管理中心
	 * @throws IOException
	 * @throws ServletException
	 */
	@RequestMapping("/managerLogin")
	public void login(String empId, String password, String deptId, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		ManagerEmp managerEmp = loginService.login(empId);
		if (managerEmp == null) {
			// 员工不存在
			request.setAttribute("message", "该员工不存在！");
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		} else {
			if (!password.equals(managerEmp.getPassword())) {
				// 密码错误
				request.setAttribute("message", "密码错误!");
				request.getRequestDispatcher("/error.jsp").forward(request, response);
			} else if (!deptId.equals(managerEmp.getDeptId())) {
				// 部门编号不正确
				request.setAttribute("message", "你不是该部门员工!");
				request.getRequestDispatcher("/error.jsp").forward(request, response);
			} else {
				// 登陆成功
				String token = UUID.randomUUID().toString();
				EmpToken empToken = new EmpToken();
				empToken.setEmpId(managerEmp.getEmpId());
				empToken.setName(managerEmp.getEmpName());
				empToken.setPhoto(managerEmp.getPhoto());
				empToken.setToken(token);

				// 去emp_token表查看是否有这个用户的token信息
				EmpToken et = empTokenService.getEmpToken(empId);
				if (et == null) {
					// emp_token不存在该用户
					empTokenService.insertEmpToken(empToken);
				} else {
					// emp_token已存在该用户
					empTokenService.updateToken(token, empId);
				}
				CookieUtils.setCookie(request, response, "token", token);
				if ("rsf".equals(deptId)) {
					// 财务部
					response.sendRedirect("../manager/html/mainFinance.html");
				} else if ("rso".equals(deptId)) {
					// 运营部
					response.sendRedirect("../manager/html/mainOperation.html");
				} else if ("rsp".equals(deptId)) {
					// 人事部
					response.sendRedirect("../manager/html/mianPersonnel.html");
				}

			}

		}
	}

	/** 
	* @Title: citylogin 
	* @Description: TODO(城市登录) 
	* @param province 省份
	* @param city 城市
	* @param empId 员工编号
	* @param password 密码
	* @param request 请求
	* @param response 响应
	* @throws IOException 
	* @throws ServletException 
	* @return void    返回类型 
	* @throws 
	*/
	@RequestMapping("/cityLogin")
	public void citylogin(String province, String city, String empId, String password,HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String result = cityService.getCityByName(city, province);
		ManagerEmp managerEmp = loginService.login(empId);

		if (managerEmp == null) {
			// 员工不存在
			request.setAttribute("message", "该员工不存在！");
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		} else {
			if (!password.equals(managerEmp.getPassword())) {
				// 密码错误
				request.setAttribute("message", "密码错误!");
				request.getRequestDispatcher("/error.jsp").forward(request, response);
			} else if (result == null) {
				// 不存在该部门
				request.setAttribute("message", "不存在该部门！");
				request.getRequestDispatcher("/error.jsp").forward(request, response);

			} else if (!result.equals(empId)) {
				// 不是该城市的管理者！
				request.setAttribute("message", "不是该城市的管理者！");
				request.getRequestDispatcher("/error.jsp").forward(request, response);
			} else {
				// 登陆成功
				String token = UUID.randomUUID().toString();
				EmpToken empToken = new EmpToken();
				empToken.setEmpId(managerEmp.getEmpId());
				empToken.setName(managerEmp.getEmpName());
				empToken.setPhoto(managerEmp.getPhoto());
				empToken.setDept(city);
				empToken.setToken(token);

				// 去emp_token表查看是否有这个的token信息
				EmpToken et = empTokenService.getEmpToken(empId);
				if (et == null) {
					// emp_token不存在该用户
					empTokenService.insertEmpToken(empToken);
				} else {
					// emp_token已存在该用户
					empTokenService.updateToken(token, empId);
				}
				System.out.println("test");
				CookieUtils.setCookie(request, response, "token", token);
				// 城市管理中心
				response.sendRedirect("../manager/html/mainCity.html");

			}

		}
	}

	/**
	 * @Title: selectTokenUser @Description: TODO() @param request @return
	 * 参数 @return EmpToken 返回类型 @throws
	 */
	@RequestMapping("/selectEmpToken")
	@ResponseBody
	public EmpToken selectTokenUser(HttpServletRequest request) {
		String token = CookieUtils.getCookieValue(request, "token");
		EmpToken empToken = empTokenService.getEmpTokenByToken(token);
		return empToken;
	}

}
